#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dh4rm4 <dh4rm4@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/29 12:40:46 by dh4rm4            #+#    #+#              #
#    Updated: 2020/09/29 12:41:22 by dh4rm4           ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = minishell

MAIN = main.c

SRC_NAME =  $(MAIN)

GREEN = \033[32m
YELLOW = \033[33m
RED = \033[31m
WHITE = \033[37m

SRC_PATH = ./srcs/

INC_PATH = ./include/

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))

CFLAGS = -Wall -Wextra -Werror -Ofast

OBJ = $(SRC:.c=.o)

LIB_PRINTF = ./ft_printf/libftprintf.a
LIB_FT = ./libft/libft.a

all: $(NAME)

$(NAME): $(OBJ)
	@printf "$(GREEN)=$(YELLOW)=$(RED)= $(WHITE)Compilation libft\t:\t"
	@make -C ./libft re
	@echo -e "$(GREEN)DONE =$(YELLOW)=$(RED)="
	@printf "$(GREEN)=$(YELLOW)=$(RED)= $(WHITE)Compilation printf\t:\t"
	@make -C ./ft_printf re
	@echo -e "$(GREEN)DONE =$(YELLOW)=$(RED)="
	@printf "$(GREEN)=$(YELLOW)=$(RED)= $(WHITE)Compilation shell\t:\t"
	@gcc $(SRC) $(LIB_FT) $(LIB_PRINTF) -o $(NAME)
	@echo -e "$(GREEN)DONE =$(YELLOW)=$(RED)="

%.o : %.c
	@gcc -o $@ -c $< -I $(INC_PATH)

clean:
	@printf "$(GREEN)=$(YELLOW)=$(RED)= $(WHITE)Clean\t\t:\t"
	@make -C ./libft clean
	@make -C ./ft_printf clean
	@/bin/rm -f $(OBJ)
	@echo -e "$(GREEN)DONE =$(YELLOW)=$(RED)="

fclean: clean
	@printf "$(GREEN)=$(YELLOW)=$(RED)= $(WHITE)FClean\t\t:\t"
	@make -C ./libft fclean
	@make -C ./ft_printf fclean
	@/bin/rm -f $(NAME)
	@/bin/rm -f ./src/./*~
	@/bin/rm -f *~
	@/bin/rm -f ./*~
	@/bin/rm -f *#
	@echo -e "$(GREEN)DONE =$(YELLOW)=$(RED)="

re: fclean all
