/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dh4rm4 <dh4rm4@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/29 12:40:28 by dh4rm4            #+#    #+#             */
/*   Updated: 2020/09/29 15:27:17 by dh4rm4           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"


/**
   Search each dir in $PATH to find CMD's path
   return: (str) CMD's path joined with CMD
*/
char *get_cmd_path(t_mini *sh, char *cmd)
{
	/* [ToDo] MANAGE CMD'S PATH BASED ON $PATH */
	return (ft_strjoin("/bin/", cmd));
}


/**
   Execute given command
   return: (int) 0
*/
void exec_command(t_minit *sh, char **split_input)
{
	int i;
	int pid;
	char	*cmd_path;

	i = -1;
	cmd_path = get_cmd_path(sh, split_input[0]);
	pid = fork();
	if (pid == 0)
        execve(cmd_path, split_input, NULL);
    else
        waitpid(pid, NULL, 0);
	/* [ToDo] STORE RETURN VALUE FROM EXECUTED CMD ($?) */
	return (0);
}


/**
   Manage output + Execute command/builtin
   return: (int) 0
*/
int manage_command(t_mini *sh, char **split_input)
{
	/* [ToDo] MANAGE OUTPUT HERE (>>, >) */
	/* [ToDo] MANAGE BUILTIN EXECUTION or CMD EXECUTION */
	return (exec_command(sh, split_input));
}


/**
   Return: 1 if char currently used to split is a space
      else 0
*/
int time_to_execute(t_mini *sh, int i_c)
{
	return (sh->c_split[i_c] == ' ');
}


/**
   Calls split_and_execute for every splitted input to start the recursion
*/
void recursive_split(t_mini *sh, char **split_input, int i_c)
{
	int k;

	k = -1;
	while (split_input[++k])
		split_and_execute(sh, split_input[k], i_c + 1);
}


/**
   Splits given input with sh->c_split[i_c]
   And starts CMD/builtin execution if sh->c_split[i_c] is a space
 */
int split_and_execute(t_mini *sh, char *input, int i_c)
{
	char **split_input;

	split_input = ft_split(input, sh->c_split[i_c]);
	if (time_to_execute(sh, i_c))
		return manage_command(sh, split_input);
	recursive_split(sh, split_input, i_c);
}


/**
   Init main structure
*/
void init_sh(t_mini *sh)
{
	sh->c_split[0] = ';';
	sh->c_split[1] = '|';
	sh->c_split[2] = ' ';
	sh->c_split[3] = 0;
	/* [ToDo] INIT ENV BASED ON MAIN FUNCTION ARGUMENT */
}


/**
   Manage prompt + var initialisation
*/
void manage_input(void)
{
	char	*input;
	t_mini	sh;

	init_sh(&sh);
	while(get_next_line(0, &input))
	{
		/* [ToDo] MANAGE ENV VAR SUBSITUTION */
		/* [ToDo] MANAGE CMD VALIDATION */
		split_and_execute(&sh, input, 0);
		ft_putstr("\n--------------------------------\n\n");
	}
}


int main(void)
{
	manage_input();
	return (0);
}
