/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kboddez <kboddez@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 09:49:13 by kboddez           #+#    #+#             */
/*   Updated: 2020/09/29 13:42:20 by dh4rm4           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

/*
**	INCLUDE LIBS
*/
# include "../libft/libft.h"
# include "../ft_printf/libftprintf.h"


/* Process management */
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


/*
**	TEST && OTHERS MACROS
*/
# define RED "\033[31m"
# define WHITE "\033[;0m"
# define GREEN "\033[32m"
# define BLUE "\033[34m"
# define YELLOW "\033[33m"
# define ITALIC "\x1b[3m"
# define SBUG(x) ft_printf("%s\n", x)

typedef struct	s_mini
{
	char		c_split[4];
}				t_mini;


#endif
